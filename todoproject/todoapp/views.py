from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse

from .models import Todo

# Create your views here.

def index(request):
    todo_list = Todo.objects.all()
    context = {'todo_list': todo_list}
    return render(request, 'todoapp/index.html', context)

def detail(request, todo_id):
    todo = get_object_or_404(Todo, pk=todo_id)
    return render(request, 'todoapp/detail.html', {'todo': todo})